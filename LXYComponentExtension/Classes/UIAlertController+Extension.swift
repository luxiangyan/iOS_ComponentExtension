//
//  UIAlertController+Extension.swift
//  MCAPI
//
//  Created by MC on 2018/11/26.
//

import Foundation
import UIKit

extension UIAlertController {
    
    /**
     *  alert  在指定控制器上显示提示框（一个AlertAction）
     */
    public static func MCShowAlert( title: String, message: String = "", on viewController: UIViewController, confirm: ((UIAlertAction)->Void)?) {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let action = UIAlertAction.init(title: "确定", style: .cancel, handler: confirm)
        alert.addAction(action)
        viewController.present(alert, animated: true)
    }
    
    /**
     *  alert  在根视图控制器上显示提示框（一个AlertAction）
     */
    public static func MCShowAlert(title: String, message: String = "",confirm: ((UIAlertAction)->Void)?) {
        if var vc = UIApplication.shared.keyWindow?.rootViewController {
            
            if vc.isKind(of: UINavigationController.classForCoder()) {
                vc = (vc as! UINavigationController).visibleViewController!
            } else if vc.isKind(of: UITabBarController.classForCoder()) {
                vc = (vc as! UITabBarController).selectedViewController!
            }
            
            MCShowAlert(title: title, message: message, on: vc, confirm: confirm)
        }
    }
    
    
    /**
     *  alert  在指定控制器上显示确认框（两个AlertAction）
     */
    public static func MCShowConfirm( title: String , message: String = "", on vc: UIViewController , confirm: ((UIAlertAction)->Void)?,cancel: ((UIAlertAction)->Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: cancel))
        alert.addAction(UIAlertAction(title: "确定", style: .default, handler: confirm))
        vc.present(alert, animated: true)
    }
    
    /**
     *  alert  在根视图控制器上显示确认框（两个AlertAction）
     */
    public static func MCShowConfirm( title: String ,message: String = "", confirm: ((UIAlertAction)->Void)?,cancel: ((UIAlertAction)->Void)? = nil) {
        
        if var vc = UIApplication.shared.keyWindow?.rootViewController {
            
            if vc.isKind(of: UINavigationController.classForCoder()) {
                vc = (vc as! UINavigationController).visibleViewController!
            } else if vc.isKind(of: UITabBarController.classForCoder()) {
                vc = (vc as! UITabBarController).selectedViewController!
            }
            
            MCShowConfirm(title: title, message: message, on: vc, confirm: confirm, cancel: cancel)
        }
    }
    
    
    /**
     *  actionSheet  在指定控制器上显示actionSheet（AlertAction根据数组数量决定）
     */
    public static func MCShowActionSheetCustom( title: String ,message: String? = nil, on vc : UIViewController, items:[String],confirm : ((Int,String) -> Void)?, cancel: ((UIAlertAction)->Void)? = nil) {
        
        let alter = UIAlertController.init(title: title, message: message, preferredStyle: .actionSheet)
        
        let cancle = UIAlertAction.init(title: "取消", style: .cancel, handler: cancel)
        
        var index = 0
        for item in items {
            let i = index
            let confirm = UIAlertAction.init(title: item, style: UIAlertAction.Style.default) { (b) in
                confirm?(i,item)
            }
            alter.addAction(confirm)
            index += 1
        }
        alter.addAction(cancle)
        vc.present(alter, animated: true, completion: nil)
    }
    
    
    
    
    /**
     * actionSheet  在指定控制器上显示拍照或者相册
     * vc : 控制器
     * hander: 闭包，选取的item
     *
     */
    public static func MCShowActionSheet_pictureWay(on vc : UIViewController,hander : ((String) -> Void)?) {
        
        let alter = UIAlertController.init(title: "选择图片", message: nil, preferredStyle: .actionSheet)
        
        let cancle = UIAlertAction.init(title: "取消", style: .cancel) { (b) in
            hander?("取消")
        }
        
        let photos = UIAlertAction.init(title: "拍照", style: .default) { (b) in
            hander?("拍照")
        }
        
        let libraries = UIAlertAction.init(title: "相册", style: .default) { (b) in
            hander?("相册")
        }
        
        alter.addAction(cancle)
        alter.addAction(photos)
        alter.addAction(libraries)
        
        vc.present(alter, animated: true, completion: nil)
    }
    
    
    
    
    /**
     * 微信/朋友圈的ActionSheet  在指定控制器上显示拍照或者相册
     * vc : 控制器
     * hander: 闭包，选取的item
     *
     */
    public static func MCShowActionSheet_shareWay(on vc : UIViewController,hander : ((String) -> Void)?) {
        
        let alter = UIAlertController.init(title: "分享", message: nil, preferredStyle: .actionSheet)
        
        let cancle = UIAlertAction.init(title: "取消", style: .cancel) { (b) in
            hander?("取消")
        }
        
        let wx = UIAlertAction.init(title: "微信好友", style: .default) { (b) in
            hander?("微信好友")
        }
        
        let pyq = UIAlertAction.init(title: "微信朋友圈", style: .default) { (b) in
            hander?("微信朋友圈")
        }
        
        alter.addAction(cancle)
        alter.addAction(wx)
        alter.addAction(pyq)
        
        vc.present(alter, animated: true, completion: nil)
    }
}


