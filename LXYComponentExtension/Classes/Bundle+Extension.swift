//
//  Bundle+Extension.swift
//  MCAPI
//
//  Created by MC on 2018/11/26.
//

import Foundation
import UIKit


@objc extension Bundle {
//    
//    /**
//     * 获取bundle的路径。
//     */
//    @objc public func MCGetBundlePath(bundleName:String) -> String {
//        let bundlePath = self.resourcePath!.appending("/\(bundleName).bundle")
//        return bundlePath
//    }
//    
//    /**
//     * 加载指定bundle路径下的图片
//     */
//    @objc public func MCLoadImageFromBundleName(_ bundleName:String,imageName:String) -> UIImage {
//        let resource_bundle = Bundle.init(path: self.MCGetBundlePath(bundleName: bundleName))
//        let image = UIImage.init(named: imageName, in: resource_bundle, compatibleWith: nil)
//        return image ?? UIImage.init()
//    }
//    
//    
    
    /**
     * 加载指定bundle下的图片资源
     */
    public static func MCLoadImageFromBundleName(_ bundleName:String, podName:String, imageName: String) -> UIImage? {
        
        
        var associateBundleURL = Bundle.main.url(forResource: "Frameworks", withExtension: nil)
        associateBundleURL = associateBundleURL?.appendingPathComponent(podName)
        associateBundleURL = associateBundleURL?.appendingPathExtension("framework")
       
        
        if associateBundleURL == nil {
            print("获取bundle失败")
            return nil
        }

        
        let associateBunle = Bundle.init(url: associateBundleURL!)
        associateBundleURL = associateBunle?.url(forResource: bundleName, withExtension: "bundle")
        let bundle = Bundle.init(url: associateBundleURL!)
        let scale = Int(UIScreen.main.scale)
        
        // 适配2x还是3x图片
        let name = imageName + "@" + String(scale) + "x"
        let path = bundle?.path(forResource: name, ofType: "png")
        
        if path == nil {
            print("获取bundle失败")
            return nil
        }
        
        let image1 = UIImage.init(contentsOfFile: path!)
        return image1
    }
}


