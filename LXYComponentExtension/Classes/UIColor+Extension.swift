//
//  UIColor+Extension.swift
//  MCAPI
//
//  Created by MC on 2018/11/26.
//

import Foundation


import UIKit




extension UIColor {
    
    /**
     * 一级标题
     */
    public static let MCBlackOne        = UIColor.MCHex("333333")
    
    /**
     * 次级标题
     */
    public static let MCBlackTwo        = UIColor.MCHex("666666")
    
    /**
     * 辅助标题
     */
    public static let MCBlackThree      = UIColor.MCHex("999999")
    
    /**
     * 未选中的颜色
     */
    public static let MCUncheck          = UIColor.MCHex("DADADA")
    
    /**
     * 分隔条 (8px)
     */
    public static let MCBar              = UIColor.MCHex("f2f2f2")
    
    /**
     * 分割线 & 页面背景底色
     */
    public static let MCLine             = UIColor.MCHex("f6f6f6")
    
    /**
     * PlaceHolder颜色
     */
    public static let MCPlaceHolder      = UIColor.MCHex("CACACC")
    
    
    /**
     * RGB颜色
     */
    public static func MCRGB(r: CGFloat,g: CGFloat,b: CGFloat) -> UIColor {
        if #available(iOS 10.0, *) {
            return UIColor(displayP3Red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1)
        } else {
            // Fallback on earlier versions
            return UIColor.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1)
        }
    }
    
    
    /**
     * 16进制的生成颜色
     */
    public class func MCHex(_ hex: String, _ alpha: CGFloat = 1.0) -> UIColor {
        if hex.isEmpty {
            return UIColor.clear
        }
        
        /** 传进来的值。 去掉了可能包含的空格、特殊字符， 并且全部转换为大写 */
        var hHex = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        /** 如果处理过后的字符串少于6位 */
        if hHex.count < 6 {
            return UIColor.clear
        }
        /** 开头是用0x开始的 */
        if hHex.hasPrefix("0X") {
            hHex = (hHex as NSString).substring(from: 2)
        }
        /** 开头是以＃开头的 */
        if hHex.hasPrefix("#") {
            hHex = (hHex as NSString).substring(from: 1)
        }
        /** 开头是以＃＃开始的 */
        if hHex.hasPrefix("##") {
            hHex = (hHex as NSString).substring(from: 2)
        }
        /** 截取出来的有效长度是6位， 所以不是6位的直接返回 */
        if hHex.count != 6 {
            return UIColor.clear
        }
        
        /** R G B */
        var range = NSMakeRange(0, 2)
        
        /** R */
        let rHex = (hHex as NSString).substring(with: range)
        
        /** G */
        range.location = 2
        let gHex = (hHex as NSString).substring(with: range)
        
        /** B */
        range.location = 4
        let bHex = (hHex as NSString).substring(with: range)
        
        /** 类型转换 */
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        
        Scanner(string: rHex).scanHexInt32(&r)
        Scanner(string: gHex).scanHexInt32(&g)
        Scanner(string: bHex).scanHexInt32(&b)
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: alpha)
    }
}



extension UIColor {
    
    /**
     * 通过颜色生成图片
     */
    public func MCCreateImage() -> UIImage {
        let rect = CGRect.init(x: 0.0, y: 0.0, width: 6.0, height: 6.0)
        UIGraphicsBeginImageContext(rect.size)
        let context : CGContext = UIGraphicsGetCurrentContext()!
        context.setFillColor(self.cgColor)
        context.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}





// 渐变色
extension CAGradientLayer {
    
    // 渐变方向
    public enum CAGradientDirection {
        case vertical
        case horizontal
    }
    
    /**
     * 使用说明 一定要设置frame
     let gradientLayer = CAGradientLayer()
     let cgColors = [UIColor.white.cgColor,UIColor.red.cgColor]
     gradientLayer = gradientLayer.createLayer(cgColors: cgColors, direction: .horizontal)
     gradientLayer.frame = CGRect.init(x: 0, y: 0, width: 200, height: 30)
     self.layer.insertSublayer(gradientLayer, at: 0)
     */
    
    /**
     * 参数说明
     * cgColors CGColor类型数组
     * direction CAGradientDirection枚举，表示颜色渐变方向
     * locations 每个颜色对应的位置，区间在[0-1]之间.比如三种颜色的集合[A,B,C],设置的区间为[0, 0.2, 1]
     那么A和B均分前0~0.2区间的位置颜色，B和C均分[0.2~1]区间的位置颜色
     */
    public class func MCCreateLayer(cgColors:[CGColor], direction: CAGradientDirection, locations: [NSNumber] = []) -> CAGradientLayer {
        
        let gradientLayer = CAGradientLayer.init()
        
        
        let gradientColors = cgColors
        
        var gradientLocations:[NSNumber] = locations
        
        if locations.isEmpty {
            let arrayM = NSMutableArray()
            let spacing = 1 / Double((cgColors.count - 1))
            
            for i in 0..<cgColors.count {
                let location = Double(i) * spacing
                arrayM.add(NSNumber.init(value: location))
            }
            gradientLocations = arrayM as! [NSNumber]
        }
        
        
        
        //创建CAGradientLayer对象并设置参数
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        
        //设置渲染的起始结束位置（横向渐变）
        if direction == .horizontal {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        } else {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        }
        return gradientLayer
    }
}

