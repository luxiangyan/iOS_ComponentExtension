#
# Be sure to run `pod lib lint LXYComponentExtension.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LXYComponentExtension'
  s.version          = '0.0.1'
  s.summary          = '集成了所有类型数据的封装'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitee.com/luxiangyan'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'lxy' => '1556937213@qq.com' }
  s.source           = { :git => 'https://gitee.com/luxiangyan/iOS_ComponentExtension.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
  
  s.swift_version = '4.2'

  # 扩展
  s.subspec 'UIView' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/UIView+Extension.swift"
  end
  
  
  s.subspec 'Bundle' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/Bundle+Extension.swift"
  end
  
  s.subspec 'String' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/String+Extension.swift"
  end
  
  s.subspec 'Int' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/Int+Extension.swift"
  end
  
  
  s.subspec 'UIAlertController' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/UIAlertController+Extension.swift"
  end
  
  s.subspec 'UIColor' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/UIColor+Extension.swift"
  end
  
  s.subspec 'UIDevice' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/UIDevice+Extension.swift"
  end
  
  s.subspec 'UIFont' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/UIFont+Extension.swift"
  end
  
  s.subspec 'UIImage' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/UIImage+Extension.swift"
      ss.dependency 'LXYComponentExtension/Bundle'
  end
  
  s.subspec 'UserDefaults' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/UserDefaults+Extension.swift"
  end
  
  s.subspec 'UIBarButtonItem' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/UIBarButtonItem+Extension.swift"
  end
  
  
  s.subspec 'MCLoading' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/MCLoading/*.swift"
  end
  
  
  s.subspec 'DispatchQueue' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/DispatchQueue+Extension.swift"
  end
  
  s.subspec 'NotificationCenter' do |ss|
      ss.source_files = "LXYComponentExtension/Classes/Extension+NotificationCenter.swift"
  end
  
  
  s.ios.resource_bundle = { 'LXYComponentExtensionBundle' => 'LXYComponentExtension/Assets/**/*.*' }
  
  # s.resource_bundles = {
  #   'LXYComponentExtension' => ['LXYComponentExtension/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
